# -------------------------------------------------
# Project created by QtCreator 2009-06-15T17:58:00
# -------------------------------------------------
QT += network
TARGET = QWol
TEMPLATE = app

DESTDIR = bin
OBJECTS_DIR = build
MOC_DIR = build
UI_DIR = build

SOURCES +=  src/mainwindow.cpp \
            src/main.cpp
HEADERS +=  src/mainwindow.h
FORMS +=    ui/mainwindow.ui

include(./libs/qtsingleapplication-2.6-opensource/src/qtsingleapplication.pri)
