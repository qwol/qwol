#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "ui_mainwindow.h"

#include <QUdpSocket>
#include <QRegExp>

class MainWindow : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_wakeButton_clicked();

private:
    QUdpSocket *udpSocket;
};

#endif // MAINWINDOW_H


// ^[0-9a-fA-F]{2}[-:]?[0-9a-fA-F]{2}[-:]?[0-9a-fA-F]{2}[-:]?[0-9a-fA-F]{2}[-:]?[0-9a-fA-F]{2}[-:]?[0-9a-fA-F]{2}$
