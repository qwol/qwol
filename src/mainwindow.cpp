#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setupUi(this);

    // Configure menu shortcuts
    connect(this->action_Quit, SIGNAL(triggered()), qApp, SLOT(quit()));            // quit() always return 0 to exec(), exit() can return an other value
    connect(this->action_About_Qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

    udpSocket = new QUdpSocket(this);
}

MainWindow::~MainWindow()
{

}

void MainWindow::on_wakeButton_clicked()
{
    // Check the validity of the MAC encoded
    QRegExp rx("^([0-9a-fA-F]{2})[-:]?([0-9a-fA-F]{2})[-:]?([0-9a-fA-F]{2})[-:]?([0-9a-fA-F]{2})[-:]?([0-9a-fA-F]{2})[-:]?([0-9a-fA-F]{2})$");
    if (rx.indexIn(hostLine->text())) {
         Ui_MainWindow::statusBar->showMessage(trUtf8("Bad MAC address format !"), 5000);
         hostLine->selectAll();
         return;
    }

    // Take the MAC address
    QByteArray mac_dst;
    for (int i = 1; i <= 6; i++)
        mac_dst.append(rx.cap(i));

    // Need 6 times 0xff at the beginning of the Magic Sequence followed by 16 times the desired MAC address
    QByteArray magic_sequence = QByteArray::fromHex("ffffffffffff");
    for (int i = 0; i < 16; i++)
        magic_sequence.append(QByteArray::fromHex(mac_dst));

    // Send the broadcast packet with the Magic Sequence
    udpSocket->writeDatagram(magic_sequence.data(), magic_sequence.size(), QHostAddress::Broadcast, 40000);

    // Notify event in the statusbar
    Ui_MainWindow::statusBar->showMessage(trUtf8("Sending the Magic Packet ..."), 5000);
}
