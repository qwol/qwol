#include <QtSingleApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QtSingleApplication a(argc, argv);
    MainWindow w;

    if (a.sendMessage("Am I alone ?"))
        return 0;

    // Set the widget which need to be visible when a second instance is trying to start
    a.setActivationWindow(&w);
    //QObject::connect(&a, SIGNAL(messageReceived(const QString&)), &w, SLOT(raise()));

    w.show();
    return a.exec();
}
